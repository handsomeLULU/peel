import Vue from 'vue';
import Router from 'vue-router';

import SiteLayout from '@/components/SiteLayout';
import Home from '@/components/site-components/Home';
import About from '@/components/site-components/About';
import Login from '@/components/site-components/Login';
import Register from '@/components/site-components/Register';
import englishhome from '@/components/site-components/englishhome';
import englishregister from '@/components/site-components/englishregister';
import englishlogin from '@/components/site-components/englishlogin';

import Announcement from '@/components/app-components/Announcement';
import FieldList from '@/components/app-components/FieldList';
import SiteDetails from '@/components/app-components/SiteDetails';
import ncyupage from '@/components/app-components/ncyupage';
import aganpage from '@/components/app-components/aganpage';
import SiteConfig from '@/components/app-components/SiteConfig';
import SiteControls from '@/components/app-components/SiteControls';

import UserList from '@/components/app-components/UserList';

import store from '@/store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: SiteLayout,
      children: [
        {
          path: ':sensorId/details',
          name: 'SiteDetails',
          component: SiteDetails,
        },
        {
          path: ':sensorId/ncyu',
          name: 'ncyu' ,
          component: ncyupage,
        },
        {
          path: ':sensorId/agan',
          name: 'agan' ,
          component: aganpage,
        },
        {
          path: '',
          name: 'Home',
          component: Home,
        }, {
          path: 'about',
          name: 'About',
          component: About,
        }, {
          path: 'announcement',
          name: 'Announcement',
          component: Announcement,
          meta: { requireAuth: true },
        }, {
          path: 'login',
          name: 'Login',
          component: Login,
        }, {
          path: 'register',
          name: 'Register',
          component: Register,
        }, {
          path: 'englishhome',
          name: 'englishhome',
          component: englishhome,
        }, {
          path: 'englishregister',
          name: 'englishregister',
          component: englishregister,
        }, {
          path: 'englishlogin',
          name: 'englishlogin',
          component: englishlogin,
        }
      ],
    }, {
      path: '/user',
      component: SiteLayout,
      meta: { requireAuth: true },
      // children inherits route guard from parent :)
      children: [
        {
          path: ':username/sites',
          name: 'FieldList',
          component: FieldList,
        }, {
          path: ':username/site/:sensorId/details',
          name: 'SiteDetails',
          component: SiteDetails,
        }, {
          path: ':username/site/:sensorId/params',
          name: 'SiteConfig',
          component: SiteConfig,
        }, {
          path: ':username/site/:sensorId/controls',
          name: 'SiteControls',
          component: SiteControls,
        },
      ],
    }, {
      path: '/admin',
      component: SiteLayout,
      meta: { requireAuth: true },
      children: [{
        path: 'users',
        name: 'UserList',
        component: UserList,
      }, {
        path: 'announcements',
        name: 'EditAnnc',
        component: Announcement,
      }],
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireAuth)) {
    if (!store.state.isLoggedIn) {
      next({ name: 'Login' });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
