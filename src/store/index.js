import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import Axios from 'axios';

Vue.use(Vuex);

const LOGIN = 'LOGIN';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGOUT = 'LOGOUT';
const SENSOR_SELECTED = 'SENSOR_SELECTED';
const SENSOR_DESELECTED = 'SENSOR_DESELECTED';

const API_HOST = process.env.API_HOST || 'localhost';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

Axios.defaults.headers.common.Authorization = localStorage.getItem('jwtToken');
// prevent xml parsing error
Axios.defaults.responseType = 'json';

export const axios = Axios.create({
  baseURL: `http://${API_HOST}:3000`,
//baseURL:'http://220.134.169.165:3000',
});
export default new Vuex.Store({
  // like data
  state: {
    isLoggedIn: !!localStorage.getItem('jwtToken'),
    user: { role: '' },
    sensorId: 0,
  },
  // like life cycle hooks
  mutations: {
    [LOGIN](state) {
      state.pending = true;
    },
    [LOGIN_SUCCESS](state, user) {
      state.isLoggedIn = true;
      state.user = user;
      Axios.defaults.headers.common.Authorization = localStorage.getItem('jwtToken');
      // all done, don't wait
      state.pending = false;
    },
    [LOGOUT](state) {
      state.isLoggedIn = false;
      state.user = { role: '' };
      state.sensorId = 0;
    },
    // stor sensorId here so we can access across views
    [SENSOR_SELECTED](state, id) {
      state.sensorId = id;
    },
    [SENSOR_DESELECTED](state) {
      state.sensorId = 0;
    },
  },
  // like methods
  actions: {
    // actions for users
    setUser({ commit }, data) {
      commit(LOGIN);
      return new Promise((resolve, reject) => {
        if (data.success === false) return reject();
        localStorage.setItem('jwtToken', `Bearer ${data.token}`);
        commit(LOGIN_SUCCESS, data.user);
        return resolve();
      });
    },
    removeToken({ commit }) {
      localStorage.removeItem('jwtToken');
      commit(LOGOUT);
    },
    // actions for sensorId selection
    selectSensor({ commit }, id) {
      commit(SENSOR_SELECTED, id);
    },
    deselectSensor({ commit }) {
      commit(SENSOR_DESELECTED);
    },
  },
  // like computed
  getters: {
    getLoginState(state) {
      return state.isLoggedIn;
    },
    getUser(state) {
      return state.user;
    },
    getSensorId(state) {
      return state.sensorId;
    },
  },
  plugins: [vuexLocal.plugin],
});
