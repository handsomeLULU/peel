FROM node:8.12.0-alpine as build-stage

# install deps
WORKDIR /home/webapp
 
COPY karotte-peel/package*.json ./
RUN npm install

# build app
COPY karotte-peel .
RUN npm run build

# real container served by nginx
FROM nginx:1.13.12-alpine as prod-stage
COPY --from=build-stage /home/webapp/dist /usr/share/nginx/html
COPY ./rabbit/nginx-default.conf /etc/nginx/conf.d/default.conf

EXPOSE 81

CMD [ "nginx", "-g", "daemon off;" ]
